package declean.guice.service.spi;

import declean.guice.service.FlyService;

public class DefaultFlyService implements FlyService {

    public void fly() {
        System.out.println("This is DefaultFlyService !");
    }

}
