package declean.guice.model;

import com.google.inject.Inject;

import declean.guice.service.FlyService;

public class Seagull {

    @Inject
    private FlyService flyService;

    @Inject
    private Countor countor;

    @Inject
    private Money money;

    public void fly() {
        flyService.fly();
    }

    public FlyService getFlyService() {
        return flyService;
    }

    public Countor getCountor() {
        return countor;
    }

    public Money getMoney() {
        return money;
    }

}
