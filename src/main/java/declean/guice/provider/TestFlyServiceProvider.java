package declean.guice.provider;

import com.google.inject.Provider;

import declean.guice.service.FlyService;
import declean.guice.service.spi.TestFlyService;

public class TestFlyServiceProvider implements Provider<FlyService> {

    public FlyService get() {
        return new TestFlyService();
    }

}
