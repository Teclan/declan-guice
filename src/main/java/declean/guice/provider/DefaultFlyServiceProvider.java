package declean.guice.provider;

import com.google.inject.Provider;

import declean.guice.service.FlyService;
import declean.guice.service.spi.DefaultFlyService;

public class DefaultFlyServiceProvider implements Provider<FlyService> {

    public FlyService get() {
        return new DefaultFlyService();
    }

}
