package declean.guice.Module;

import com.google.inject.AbstractModule;

import declean.guice.provider.DefaultFlyServiceProvider;
import declean.guice.service.FlyService;

public class DefaultFlyServiceModuleWithProvider extends AbstractModule {

    @Override
    protected void configure() {

        // 使用 Provider 绑定数据
        bind(FlyService.class).toProvider(DefaultFlyServiceProvider.class);
    }

}
