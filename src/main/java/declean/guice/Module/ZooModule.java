package declean.guice.Module;

import com.google.inject.AbstractModule;

import declean.guice.model.Countor;
import declean.guice.model.Money;
import declean.guice.provider.MoneyProvider;
import declean.guice.service.FlyService;
import declean.guice.service.spi.DefaultFlyService;

public class ZooModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Countor.class).asEagerSingleton();

        bind(FlyService.class).to(DefaultFlyService.class);

        // 绑定方式 一,使用 MoneyProvider 的方式二 才达到单例效果
        bind(Money.class).toProvider(MoneyProvider.class);

        // 绑定方式 二
        // bind(Money.class).toProvider(MoneyProvider.class).asEagerSingleton();
    }

}
