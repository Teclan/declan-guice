package declean.guice.Module;

import com.google.inject.AbstractModule;

import declean.guice.model.Money;
import declean.guice.provider.MoneyProvider;

public class MoneyModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Money.class).toProvider(MoneyProvider.class);
    }

}
