package declean.guice.inject;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import declean.guice.Module.DefaultFlyModule;
import declean.guice.Module.DefaultFlyServiceModuleWithProvider;
import declean.guice.Module.TestFlyModule;
import declean.guice.Module.TestFlyServiceModuleWithProvider;
import declean.guice.model.Seagull;
import declean.guice.service.spi.DefaultFlyService;
import declean.guice.service.spi.TestFlyService;

public class SeagullTest {

    @Test
    public void defaultFlyTest() {
        Injector injector = Guice.createInjector(new DefaultFlyModule());
        Seagull seagull = injector.getInstance(Seagull.class);
        System.out.print("defaultFlyTest:");
        seagull.fly();

        assertTrue(DefaultFlyService.class
                .equals(seagull.getFlyService().getClass()));
    }

    @Test
    public void testFlyTest() {
        Injector injector = Guice.createInjector(new TestFlyModule());
        Seagull seagull = injector.getInstance(Seagull.class);
        System.out.print("testFlyTest:");
        seagull.fly();

        assertTrue(TestFlyService.class
                .equals(seagull.getFlyService().getClass()));
    }

    @Test
    public void bindDefaultFlyServiceWithProviderTest() {

        Injector injector = Guice
                .createInjector(new DefaultFlyServiceModuleWithProvider());

        Seagull seagull = injector.getInstance(Seagull.class);

        System.out.print("bindDefaultFlyServiceWithProviderTest:");
        seagull.fly();
        assertTrue(DefaultFlyService.class
                .equals(seagull.getFlyService().getClass()));
    }

    @Test
    public void bindTestFlyServiceWithProviderTest() {

        Injector injector = Guice
                .createInjector(new TestFlyServiceModuleWithProvider());
        Seagull seagull = injector.getInstance(Seagull.class);
        System.out.print("bindTestFlyServiceWithProviderTest:");
        seagull.fly();
        assertTrue(TestFlyService.class
                .equals(seagull.getFlyService().getClass()));
    }

}
