package declean.guice.inject;

import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import declean.guice.Module.ZooModule;
import declean.guice.model.Zoo;

public class ZooTest {

    @Test
    public void zooTest() {
        Injector injector = Guice.createInjector(new ZooModule());
        Zoo zoo = injector.getInstance(Zoo.class);

        zoo.fly();

        zoo.getSeagull().getCountor().show();
        zoo.getSeagull().getCountor().add(100).show();
        zoo.getSeagull().getCountor().show();
        zoo.getEagle().getCountor().show();

        zoo.getSeagull().getMoney().show();
        zoo.getSeagull().getMoney().add(55).show();
        zoo.getSeagull().getMoney().show();
        zoo.getEagle().getMoney().show();
    }

}
