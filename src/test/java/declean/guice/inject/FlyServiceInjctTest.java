package declean.guice.inject;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

import declean.guice.Module.DefaultFlyModule;
import declean.guice.Module.DefaultFlyServiceModuleWithProvider;
import declean.guice.Module.TestFlyModule;
import declean.guice.Module.TestFlyServiceModuleWithProvider;
import declean.guice.service.FlyService;
import declean.guice.service.spi.DefaultFlyService;
import declean.guice.service.spi.TestFlyService;

public class FlyServiceInjctTest {

    @Test
    public void bindDefaultFlyServiceTest() {

        Injector injector = Guice.createInjector(new DefaultFlyModule());
        FlyService service = injector.getInstance(FlyService.class);
        System.out.print("bindDefaultFlyServiceTest:");
        service.fly();
        assertTrue(DefaultFlyService.class.equals(service.getClass()));
    }

    @Test
    public void bindTestFlyServiceTest() {

        Injector injector = Guice.createInjector(new TestFlyModule());
        FlyService service = injector.getInstance(FlyService.class);
        System.out.print("bindTestFlyServiceTest:");
        service.fly();
        assertTrue(TestFlyService.class.equals(service.getClass()));
    }

    @Test
    public void bindDefaultFlyServiceWithProviderTest() {

        Injector injector = Guice
                .createInjector(new DefaultFlyServiceModuleWithProvider());
        FlyService service = injector.getInstance(FlyService.class);
        System.out.print("bindDefaultFlyServiceWithProviderTest:");
        service.fly();
        assertTrue(DefaultFlyService.class.equals(service.getClass()));
    }

    @Test
    public void bindTestFlyServiceWithProviderTest() {

        Injector injector = Guice
                .createInjector(new TestFlyServiceModuleWithProvider());
        FlyService service = injector.getInstance(FlyService.class);
        System.out.print("bindTestFlyServiceWithProviderTest:");
        service.fly();
        assertTrue(TestFlyService.class.equals(service.getClass()));
    }

}
